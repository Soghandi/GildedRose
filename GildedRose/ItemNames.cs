﻿namespace GildedRose;

public static class ItemNames
{
    public static string AgedBrie =  "Aged Brie";
    public static string BackStage = "Backstage passes to a TAFKAL80ETC concert";
    public static string Sulfuras = "Sulfuras, Hand of Ragnaros";
    public static string Conjured = "Conjured Mana Cake";
}