﻿using System.Collections.Generic;
using GildedRose.Models;

namespace GildedRose
{
    public class GildedRose
    {
        IList<SmartItem> Items;

        public const int MinimumQuality = 0;
        public const int MaximumQuality = 50;

        public GildedRose(IList<SmartItem> items)
        {
            this.Items = items;
        }

        /// <summary>
        /// Update Quality (new method)
        /// </summary>
        public void UpdateQuality()
        {
            foreach (var item in Items)
            {
                item.UpdateQuality();
            }
        }
        
        /// <summary>
        /// 0bselete method (we can delete it)
        /// </summary>
        public void OldUpdateQuality()
        {
            foreach (var item in Items)
            {
                if (item.Name != ItemNames.AgedBrie && item.Name != ItemNames.BackStage)
                {
                    if (item.Quality > 0)
                    {
                        if (item.Name != ItemNames.Sulfuras)
                        {
                            item.Quality -= 1;
                        }
                        if (item.Name == ItemNames.Conjured && item.Quality > 0)
                        {
                            item.Quality -= 1;
                        }
                    }
                }
                else
                {
                    if (item.Quality < 50)
                    {
                        item.Quality += 1;

                        if (item.Name == ItemNames.BackStage)
                        {
                            if (item.SellIn < 11)
                            {
                                if (item.Quality < 50)
                                {
                                    item.Quality += 1;
                                }
                            }

                            if (item.SellIn < 6)
                            {
                                if (item.Quality < 50)
                                {
                                    item.Quality += 1;
                                }
                            }
                        }
                    }
                }

                if (item.Name !=  ItemNames.Sulfuras)
                {
                    item.SellIn -=  1;
                }

                if (item.SellIn < 0)
                {
                    if (item.Name !=  ItemNames.AgedBrie)
                    {
                        if (item.Name !=  ItemNames.BackStage)
                        {
                            if (item.Quality > 0)
                            {
                                if (item.Name !=  ItemNames.Sulfuras)
                                {
                                    item.Quality -= 1;
                                }
                            }
                        }
                        else
                        {
                            item.Quality = 0;
                        }
                    }
                    else
                    {
                        if (item.Quality < 50)
                        {
                            item.Quality +=  1;
                        }
                    }
                }
            }
        }
    }
}
