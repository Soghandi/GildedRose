﻿namespace GildedRose.Models;

public class ConjuredItem : SmartItem
{
    public ConjuredItem()
    {
        Name = ItemNames.Conjured;
        QualityAddedValue = -2;
    }
    protected override void RunCustomLogics()
    {
        
    }
}