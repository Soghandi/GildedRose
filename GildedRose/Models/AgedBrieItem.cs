﻿namespace GildedRose.Models;

public class AgedBrieItem : SmartItem
{
    public AgedBrieItem()
    {
        Name = ItemNames.AgedBrie;
        QualityAddedValue = 1;
    }

    protected override void RunCustomLogics()
    {
        if (SellIn < 0 && Quality < 50)
        {
            Quality += 1;
        }
    }
}