﻿namespace GildedRose.Models;

public class BackStageItem : SmartItem
{
    public BackStageItem()
    {
        Name = ItemNames.BackStage;
        QualityAddedValue = 1;
    }

    protected override void RunCustomLogics()
    {
        if (SellIn < 10)
        {
            if (Quality < 50)
            {
                Quality += 1;
            }
        }

        if (SellIn < 5)
        {
            if (Quality < 50)
            {
                Quality += 1;
            }
        }

        if (SellIn < 0)
        {
            Quality = 0;
        }
    }
}