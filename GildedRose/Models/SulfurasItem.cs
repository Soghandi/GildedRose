﻿namespace GildedRose.Models;

public class SulfurasItem : SmartItem
{
    /// <summary>
    /// Sulfuras is a legendary item and as such its Quality never alters.
    /// </summary>
    public SulfurasItem()
    {
        Name = ItemNames.Sulfuras;
        IsLegendary = true;
    }
    protected override void RunCustomLogics()
    {
    
    }
}