﻿namespace GildedRose.Models;

public class SmartItem : Item
{
    protected bool IsLegendary { get; set; }
    protected int QualityAddedValue { get; set; }

    public void UpdateQuality()
    {
        if (IsLegendary) return;
        Quality += QualityAddedValue;
        SellIn -= 1;
        RunCustomLogics();
        if (Quality < 0) Quality = 0;
        if (Quality > 50) Quality = 50;
    }

    protected virtual void RunCustomLogics()
    {
    }
}