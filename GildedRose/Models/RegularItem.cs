﻿namespace GildedRose.Models;

public class RegularItem : SmartItem
{
    public RegularItem()
    {
        QualityAddedValue = -1;
    }
    protected override void RunCustomLogics()
    {
        if (SellIn < 0)
        {
            Quality -= 1;
        }
    }
}