﻿using Xunit;
using System.Collections.Generic;
using GildedRose;
using GildedRose.Models;

namespace GildedRoseTests
{
    public class GildedRoseTest
    {
        //	- The Quality of an item is never negative
        [Fact]
        public void QualityIsEqualOrGreaterThanMinimum()
        {
            var item = CreateGildedRose(new RegularItem() { Name = "foo", SellIn = 0, Quality = 0 });
            Assert.True(item.Quality >= GildedRose.GildedRose.MinimumQuality);
        }

        //- At the end of each day our system lowers both values for every item
        [Fact]
        public void LowerQualityAndSellInAfterOneUpdate()
        {
            var item = CreateGildedRose(new RegularItem() { Name = "foo", SellIn = 10, Quality = 20 });
            Assert.Equal(9, item.SellIn);
            Assert.Equal(19, item.Quality);
        }

        //	- Once the sell by date has passed, Quality degrades twice as fast
        [Fact]
        public void OnceSellDatePassedQualityDegradeTwice()
        {
            var item = CreateGildedRose(new RegularItem() { Name = "foo", SellIn = 0, Quality = 30 });
            Assert.Equal(28, item.Quality);
        }

        //	- "Aged Brie" actually increases in Quality the older it gets
        [Fact]
        public void AgedBrieQualityIncreasesOnce()
        {
            var item = CreateGildedRose(new AgedBrieItem() { SellIn = 10, Quality = 30 });
            Assert.Equal(31, item.Quality);
        }


        //	- The Quality of an item is never more than 50
        [Fact]
        public void QualityIsEqualOrLessThanMaximum()
        {
            var item = CreateGildedRose(new AgedBrieItem() { SellIn = 10, Quality = 50 });
            Assert.True(item.Quality <= GildedRose.GildedRose.MaximumQuality);
        }


        //	- "Sulfuras", being a legendary item, never has to be sold or decreases in Quality
        [Fact]
        public void SulfurasNeverHasToBeSoldOrDecreaseInQuality()
        {
            var item = CreateGildedRose(new SulfurasItem() { SellIn = -10, Quality = 90 });
            Assert.Equal(90, item.Quality);
            Assert.Equal(-10, item.SellIn);
        }


        //	- "Backstage passes" actually increases in Quality the older it gets
        [Fact]
        public void BackstageQualityIncrease()
        {
            var item = CreateGildedRose(new BackStageItem() { SellIn = 20, Quality = 30 });
            Assert.Equal(31, item.Quality);
        }

        //	- "Backstage passes" Quality increases by 2 when there are 10 days
        [Fact]
        public void BackstageQualityIncreasesBy2WhenIs10OrLess()
        {
            var item = CreateGildedRose(new BackStageItem() { SellIn = 10, Quality = 30 });
            Assert.Equal(32, item.Quality);
        }

        //	- "Backstage passes" Quality increases by 3 when there are 5 days
        [Fact]
        public void BackstageQualityIncreasesBy23WhenIs5OrLess()
        {
            var item = CreateGildedRose(new BackStageItem() { SellIn = 5, Quality = 30 });
            Assert.Equal(33, item.Quality);
        }

        //	- "Backstage passes" Quality drops to 0 after the concert
        [Fact]
        public void BackstageQualityDropsTo0AfterTheConcert()
        {
            var item = CreateGildedRose(new BackStageItem() { SellIn = 0, Quality = 30 });
            Assert.Equal(0, item.Quality);
        }

        //	- "Conjured" items degrade in Quality twice as fast as normal items
        [Fact]
        public void ConjuredQualityDecreasesTwice()
        {
            var item = CreateGildedRose(new ConjuredItem() { SellIn = 10, Quality = 30 });
            Assert.Equal(28, item.Quality);
        }

        [Fact]
        public void SellInShouldDecreaseAfterEachUpdate()
        {
            int sellIn = 10;
            var items = CreateGildedRose(new List<SmartItem>()
            {
                new RegularItem() { SellIn = sellIn, Quality = 30 },
                new ConjuredItem() { SellIn = sellIn, Quality = 30 },
                new AgedBrieItem() { SellIn = sellIn, Quality = 30 },
                new BackStageItem() { SellIn = sellIn, Quality = 30 },
            });
            foreach (var item in items)
            {
                Assert.Equal(sellIn - 1, item.SellIn);
            }
        }

        private SmartItem CreateGildedRose(SmartItem item)
        {
            IList<SmartItem> items = new List<SmartItem> { item };
            GildedRose.GildedRose app = new GildedRose.GildedRose(items);
            app.UpdateQuality();
            return item;
        }
        
        private IList<SmartItem> CreateGildedRose(IList<SmartItem> items)
        {
            GildedRose.GildedRose app = new GildedRose.GildedRose(items);
            app.UpdateQuality();
            return items;
        }
    }
}