﻿using System;
using System.Collections.Generic;
using GildedRose.Models;

namespace GildedRoseTests
{
    public static class TexttestFixture
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("OMGHAI!");

            IList<SmartItem> Items = new List<SmartItem>
            {
                new RegularItem() { Name = "+5 Dexterity Vest", SellIn = 10, Quality = 20 },
                new AgedBrieItem() { SellIn = 2, Quality = 0 },
                new RegularItem() { Name = "Elixir of the Mongoose", SellIn = 5, Quality = 7 },
                new SulfurasItem() { SellIn = 0, Quality = 80 },
                new SulfurasItem() { SellIn = -1, Quality = 80 },
                new BackStageItem()
                {
                    SellIn = 15,
                    Quality = 20
                },
                new BackStageItem()
                {
                    SellIn = 10,
                    Quality = 49
                },
                new BackStageItem()
                {
                    SellIn = 5,
                    Quality = 49
                },
                // this conjured item does not work properly yet
                new ConjuredItem() { SellIn = 3, Quality = 6 }
            };

            var app = new GildedRose.GildedRose(Items);

            int days = 30;
            if (args.Length > 0)
            {
                days = int.Parse(args[0]) + 1;
            }

            for (var i = 0; i < days; i++)
            {
                Console.WriteLine("-------- day " + i + " --------");
                Console.WriteLine("name, sellIn, quality");
                for (var j = 0; j < Items.Count; j++)
                {
                    System.Console.WriteLine(Items[j].Name + ", " + Items[j].SellIn + ", " + Items[j].Quality);
                }

                Console.WriteLine("");
                app.UpdateQuality();
            }
        }
    }
}